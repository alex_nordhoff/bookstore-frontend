//console.log("Hello World!");

const BACKEND_URL = "http://18.221.174.156:5050/books"
//const request = new Request(BACKEND_URL, {method: 'GET'});

async function fetchData()
{
    const request = new Request(BACKEND_URL, {method: 'GET'});
    const response = await fetch(request);
    const result = await response.json();
    //console.log(result);
    document.getElementById('table').innerHTML = "";




    let htmlCode = '<div class="container1"><div class="row">';
    for(var i = 0; i < result.length; i++){
        htmlCode+= '<div class="col-md-3 border border-white rounded">';
        htmlCode+= '<br><center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="' + getRandomImage() + '" width="110" height="150" class="mr-4 rounded"alt="Book Picture"></center>';
        htmlCode+= '<br><center><strong>'+ result[i].title + '</strong></center>';
        htmlCode+= '<h9><br><center>By: '  + result[i].author  + '</center><br><center> $'+ result[i].price + '</center></h9>';
        htmlCode += '<br><center><button type="button" class="btn btn-danger btn-sm" onclick=removeData(\'' + result[i]._id  + '\')>Delete Book</button></center>';
        htmlCode += '<br><center><button type="button" class="btn btn-info btn-sm" onclick=putData(\'' + result[i]._id + '\')>Update Book</button></center>';
        htmlCode+= '</div>';
    }
    htmlCode+= '</div></div>';
    document.getElementById('table').innerHTML += htmlCode;
}

async function postData(){

    const data = {title: '', author: '', price: 0}

    data.title = document.getElementById('title').value;
    data.author = document.getElementById('author').value;
    data.price = document.getElementById('price').value;

    const request = new Request(BACKEND_URL, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    });

    const response = await fetch(request);
    if (response.status === 400){
        console.log("It failed! Error: ");
    }

    else if (response.status === 500){
        console.log("Server Error");
    }

    const result = await response.json();
    console.log("Record Inserted Successfully!");
    console.log(result);

    fetchData();

}

async function removeData(id = ''){

    //console.log("Hello");

    const data = {id}

    const request = new Request(BACKEND_URL +'/'+ id , {
        method: 'DELETE',
        headers: {'Accept' : 'application/json',
            'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    });

    const response = await fetch(request);
    if (response.status === 400){
        console.log("It failed! Error: ");
    }
 
    else if (response.status === 500){
        console.log("Server Error");
    }

    const result = await response.json();
    console.log(result);

    fetchData();

} 

async function putData(id = ''){

    const data = {id, title: '', author: '', price: 0}

    data.title = document.getElementById('title').value;
    data.author = document.getElementById('author').value;
    data.price = document.getElementById('price').value;
    

    const request = new Request(BACKEND_URL +'/'+ id , {
        method: 'PUT',
        headers: {'Accept' : 'application/json',
            'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    });

    const response = await fetch(request);
    if (response.status === 400){
        console.log("It failed! Error: ");
    }

    else if (response.status === 500){
        console.log("Server Error");
    }

    const result = await response.json();
    console.log(result);

    fetchData();
}
